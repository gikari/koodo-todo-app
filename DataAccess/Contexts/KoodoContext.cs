using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Contexts
{
    public class KoodoContext : DbContext
    {
        public DbSet<TodoGroupEntity> TodoGroups { get; set; }
        public DbSet<TodoItemEntity> TodoItems { get; set; }

        public KoodoContext()
        {
            
        }

        public KoodoContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoGroupEntity>();
            modelBuilder.Entity<TodoItemEntity>();
            
            base.OnModelCreating(modelBuilder);
        }
    }
}