using System.Collections.Generic;
using Domain.Models;

namespace DataAccess.Contracts
{
    public interface ICrudDataAccess<TModel>
        where TModel : CrudModel
    {
        void Add(TModel model);
        void Remove(TModel model);
        void Modify(TModel model);
        IEnumerable<TModel> GetAll();
        TModel Get(int id);
        bool Contains(TModel model);
    }
}