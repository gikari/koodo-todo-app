using Domain.Models;

namespace DataAccess.Contracts
{
    public interface ITodoGroupDataAccess : ICrudDataAccess<TodoGroupModel>
    {
    }
}