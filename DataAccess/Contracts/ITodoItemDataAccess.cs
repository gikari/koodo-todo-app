using Domain.Models;

namespace DataAccess.Contracts
{
    public interface ITodoItemDataAccess : ICrudDataAccess<TodoItemModel>
    {
    }
}