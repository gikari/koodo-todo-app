﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class ItemsName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItemEntity_TodoGroupEntity_GroupId",
                table: "TodoItemEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TodoItemEntity",
                table: "TodoItemEntity");

            migrationBuilder.RenameTable(
                name: "TodoItemEntity",
                newName: "TodoItems");

            migrationBuilder.RenameIndex(
                name: "IX_TodoItemEntity_GroupId",
                table: "TodoItems",
                newName: "IX_TodoItems_GroupId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TodoItems",
                table: "TodoItems",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItems_TodoGroupEntity_GroupId",
                table: "TodoItems",
                column: "GroupId",
                principalTable: "TodoGroupEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItems_TodoGroupEntity_GroupId",
                table: "TodoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TodoItems",
                table: "TodoItems");

            migrationBuilder.RenameTable(
                name: "TodoItems",
                newName: "TodoItemEntity");

            migrationBuilder.RenameIndex(
                name: "IX_TodoItems_GroupId",
                table: "TodoItemEntity",
                newName: "IX_TodoItemEntity_GroupId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TodoItemEntity",
                table: "TodoItemEntity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItemEntity_TodoGroupEntity_GroupId",
                table: "TodoItemEntity",
                column: "GroupId",
                principalTable: "TodoGroupEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
