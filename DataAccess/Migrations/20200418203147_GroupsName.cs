﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class GroupsName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItems_TodoGroupEntity_GroupId",
                table: "TodoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TodoGroupEntity",
                table: "TodoGroupEntity");

            migrationBuilder.RenameTable(
                name: "TodoGroupEntity",
                newName: "TodoGroups");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TodoGroups",
                table: "TodoGroups",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItems_TodoGroups_GroupId",
                table: "TodoItems",
                column: "GroupId",
                principalTable: "TodoGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItems_TodoGroups_GroupId",
                table: "TodoItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TodoGroups",
                table: "TodoGroups");

            migrationBuilder.RenameTable(
                name: "TodoGroups",
                newName: "TodoGroupEntity");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TodoGroupEntity",
                table: "TodoGroupEntity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItems_TodoGroupEntity_GroupId",
                table: "TodoItems",
                column: "GroupId",
                principalTable: "TodoGroupEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
