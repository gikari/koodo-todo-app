﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class RemoveRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItems_TodoGroups_GroupId",
                table: "TodoItems");

            migrationBuilder.AlterColumn<int>(
                name: "GroupId",
                table: "TodoItems",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItems_TodoGroups_GroupId",
                table: "TodoItems",
                column: "GroupId",
                principalTable: "TodoGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItems_TodoGroups_GroupId",
                table: "TodoItems");

            migrationBuilder.AlterColumn<int>(
                name: "GroupId",
                table: "TodoItems",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItems_TodoGroups_GroupId",
                table: "TodoItems",
                column: "GroupId",
                principalTable: "TodoGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
