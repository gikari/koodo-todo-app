using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class TodoGroupEntity : CrudEntity
    {
        public string Name { get; set; }
        
        public ICollection<TodoItemEntity> TodoItems { get; set; } = new HashSet<TodoItemEntity>();
    }
}