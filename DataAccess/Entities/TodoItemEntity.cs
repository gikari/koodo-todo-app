﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class TodoItemEntity : CrudEntity
    {
        public TodoGroupEntity Group { get; set; }
        public string Contents { get; set; }
        [Required]
        public bool Done { get; set; }
    }
}