using AutoMapper;
using DataAccess.Contexts;
using DataAccess.Contracts;
using DataAccess.Entities;
using Domain.Models;
using Microsoft.Extensions.Logging;

namespace DataAccess.Implementations
{
    public class TodoGroupDataAccess :
        CrudDataAccess<TodoGroupModel, TodoGroupEntity>,
        ITodoGroupDataAccess
    {
        public TodoGroupDataAccess(
            ILogger<CrudDataAccess<TodoGroupModel, TodoGroupEntity>> logger,
            KoodoContext context,
            IMapper mapper)
            : base(logger, context, mapper)
        {
        }
    }
}