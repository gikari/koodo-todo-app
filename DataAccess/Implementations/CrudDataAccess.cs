using System.Collections.Generic;
using AutoMapper;
using DataAccess.Contexts;
using DataAccess.Contracts;
using DataAccess.Entities;
using Domain.Models;
using Microsoft.Extensions.Logging;

namespace DataAccess.Implementations
{
    public abstract class CrudDataAccess<TModel, TEntity> : ICrudDataAccess<TModel>
        where TModel : CrudModel
        where TEntity : CrudEntity
    {

        private ILogger<CrudDataAccess<TModel, TEntity>> Logger { get; }
        private KoodoContext Context { get; }
        private IMapper Mapper { get; }

        protected CrudDataAccess(ILogger<CrudDataAccess<TModel, TEntity>> logger, KoodoContext context, IMapper mapper)
        {
            Logger = logger;
            Context = context;
            Mapper = mapper;
        }
        
        public void Add(TModel model)
        {
            Context.Add(Mapper.Map<TEntity>(model));

            Context.SaveChanges();
        }

        public void Remove(TModel model)
        {
            var entityToRemove = Context.Find<TEntity>(model.Id);
            
            Logger.LogDebug($"Entity to delete: {entityToRemove}");

            if (entityToRemove != null)
            {
                Context.Remove(entityToRemove);
                
                Context.SaveChanges();
                
                Logger.LogDebug($"Entity with id {model.Id} is deleted");
            }
            else
            {
                Logger.LogError($"No entity with id {model.Id} is found");
            }
            
        }

        public void Modify(TModel model)
        {
            var entity = Mapper.Map<TEntity>(model);

            if (Context.Find<TEntity>(entity.Id) == null)
            {
                return;
            }
            
            Context.Update(entity);
            
            Context.SaveChanges();
        }

        public IEnumerable<TModel> GetAll()
        {
            var allGroups = Context.TodoGroups;

            return Mapper.Map<IEnumerable<TModel>>(allGroups);
        }

        public TModel Get(int id)
        {
            var entity = Context.Find<TEntity>(id);

            return entity != null ? Mapper.Map<TModel>(entity) : null;
        }

        public bool Contains(TModel model)
        {
            var entity = Context.Find<TEntity>(model.Id);
            
            return entity != null;
        }
    }
}