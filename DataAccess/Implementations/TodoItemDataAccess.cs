using AutoMapper;
using DataAccess.Contexts;
using DataAccess.Contracts;
using DataAccess.Entities;
using Domain.Models;
using Microsoft.Extensions.Logging;

namespace DataAccess.Implementations
{
    public class TodoItemDataAccess : CrudDataAccess<TodoItemModel, TodoItemEntity>, ITodoItemDataAccess
    {
        public TodoItemDataAccess(
            ILogger<CrudDataAccess<TodoItemModel, TodoItemEntity>> logger,
            KoodoContext context, IMapper mapper)
            : base(logger, context, mapper)
        {
        }
    }
}