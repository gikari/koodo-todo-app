using AutoMapper;
using Client.DTO;
using DataAccess.Entities;
using Domain.Models;

namespace WebAPI
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<TodoGroupDto, TodoGroupModel>().ReverseMap();
            CreateMap<TodoGroupModel, TodoGroupEntity>().ReverseMap();
            
            CreateMap<TodoItemDto, TodoItemModel>().ReverseMap();
            CreateMap<TodoItemModel, TodoItemEntity>().ReverseMap();
        }
    }
}