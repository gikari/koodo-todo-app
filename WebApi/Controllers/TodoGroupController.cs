using AutoMapper;
using BLL.Contracts;
using Client.DTO;
using DataAccess.Contracts;
using DataAccess.Entities;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/todo-group")]
    public class TodoGroupController :
        CrudController<TodoGroupDto, TodoGroupModel>
    {
        public TodoGroupController(ITodoGroupService service, IMapper mapper, ILogger<CrudController<TodoGroupDto, TodoGroupModel>> logger) : base(service, mapper, logger)
        {
        }
    }
}