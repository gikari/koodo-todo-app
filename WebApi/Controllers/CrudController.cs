using System.Collections.Generic;
using AutoMapper;
using BLL.Contracts;
using Client.DTO;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers
{
    [ApiController]
    public abstract class CrudController<TDto, TModel>
        : ControllerBase
        where TDto : CrudDto
        where TModel : CrudModel
    {
        protected IMapper Mapper { get; }
        
        protected ILogger<CrudController<TDto, TModel>> Logger { get; }
    
        protected ICrudService<TModel> Service { get; }

        protected CrudController(ICrudService<TModel> service, IMapper mapper,
            ILogger<CrudController<TDto, TModel>> logger)
        {
            Service = service;
            Mapper = mapper;
            Logger = logger;
        }

        [HttpPut]
        [Route("")]
        public string Add(TDto dto)
        {
            Logger.LogDebug("Boop! Crud Controller triggered!");
            
            Service.Add(Mapper.Map<TModel>(dto));
            return "OK";
        }

        [HttpDelete]
        [Route("")]
        public string Remove(TDto dto)
        {
            Service.Remove(Mapper.Map<TModel>(dto));

            return "OK";
        }

        [HttpPatch]
        [Route("")]
        public string Modify(TDto dto)
        {
            Service.Modify( Mapper.Map<TModel>(dto));
            
            return "OK";
        }
        
        [HttpGet]
        [Route("")]
        public IEnumerable<TDto> GetAll()
        {
            var allEntries = Service.GetAll();

            return Mapper.Map<IEnumerable<TDto>>(allEntries);
        }
        
        [HttpGet]
        [Route("{id}")]
        public TDto Get(int id)
        {
            var entry = Service.Get(id);

            return Mapper.Map<TDto>(entry);
        }
    }
}