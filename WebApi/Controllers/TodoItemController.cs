using AutoMapper;
using BLL.Contracts;
using Client.DTO;
using DataAccess.Contracts;
using DataAccess.Entities;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/todo-item")]
    public class TodoItemController :
        CrudController<TodoItemDto, TodoItemModel>
    {
        public TodoItemController(ITodoItemService service, IMapper mapper, ILogger<CrudController<TodoItemDto, TodoItemModel>> logger) : base(service, mapper, logger)
        {
        }
    }
}