namespace Domain.Models
{
    public abstract class CrudModel
    {
        public int Id { get; set; }
    }
}