﻿
namespace Domain.Models
{
    public class TodoGroupModel : CrudModel
    {
        public string Name { get; set; }
    }
}