namespace Domain.Models
{
    public class TodoItemModel : CrudModel
    {
        public TodoGroupModel Group { get; set; }
        public string Contents { get; set; }
        public bool Done { get; set; }
    }
}