using BLL.Implementations;
using DataAccess.Contracts;
using Domain.Models;
using Moq;
using NUnit.Framework;

namespace Tests.Unit.BLL
{
    public class TodoGroupServiceTests
    {
        [Test]
        public void ValidateForAddition_Unique_ReturnsTrue()
        {
            // Arrange
            var mockModel = new Mock<TodoGroupModel>();
            
            var mockDataAccess = new Mock<ITodoGroupDataAccess>();
            mockDataAccess.Setup(dataAccess => dataAccess.Contains(mockModel.Object)).Returns(false);
            
            var service = new TodoGroupService(mockDataAccess.Object, null, null);
            
            // Act
            var result = service.ValidateForAddition(mockModel.Object);
            
            // Assert
            Assert.IsTrue(result);
        }
        
        [Test]
        public void ValidateForAddition_Null_ReturnsFalse()
        {
            // Arrange
            var mockModel = new Mock<TodoGroupModel>();
            
            var mockDataAccess = new Mock<ITodoGroupDataAccess>();
            mockDataAccess.Setup(dataAccess => dataAccess.Contains(mockModel.Object)).Returns(false);
            
            var service = new TodoGroupService(mockDataAccess.Object, null, null);
            
            // Act
            var result = service.ValidateForAddition(null);
            
            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void ValidateForAddition_AlreadyExists_ReturnsFalse()
        {
            // Arrange
            var mockModel = new Mock<TodoGroupModel>();
            
            var mockDataAccess = new Mock<ITodoGroupDataAccess>();
            mockDataAccess.Setup(dataAccess => dataAccess.Contains(mockModel.Object)).Returns(true);
            
            var service = new TodoGroupService(mockDataAccess.Object, null, null);
            
            // Act
            var result = service.ValidateForAddition(mockModel.Object);
            
            // Assert
            Assert.IsFalse(result);
        }
    }
}