using System.Collections.Generic;
using AutoMapper;
using BLL.Contracts;
using DataAccess.Contracts;
using Domain.Models;
using Microsoft.Extensions.Logging;

namespace BLL.Implementations
{
    public abstract class CrudService<TModel> : ICrudService<TModel>
        where TModel : CrudModel
    {
            private ICrudDataAccess<TModel> DataAccess { get; }
            private ILogger<CrudService<TModel>> Logger;
            private IMapper Mapper { get; }

            protected CrudService(ICrudDataAccess<TModel> dataAccess, ILogger<CrudService<TModel>> logger, IMapper mapper)
            {
                DataAccess = dataAccess;
                Logger = logger;
                Mapper = mapper;
            }
    
            public void Add(TModel model)
            {
                var valid = ValidateForAddition(model);

                if (valid)
                {
                    DataAccess.Add(model);
                }
            }
    
            public void Remove(TModel model)
            {
                DataAccess.Remove(model);
            }
    
            public void Modify(TModel model)
            {
                DataAccess.Modify(model);
            }
    
            public IEnumerable<TModel> GetAll()
            {
                return DataAccess.GetAll();
            }
    
            public TModel Get(int id)
            {
                return DataAccess.Get(id);
            }

            public bool ValidateForAddition(TModel model)
            {
                if (model == null)
                {
                    return false;
                }

                var exists = DataAccess.Contains(model);
                if (exists)
                {
                    return false;
                }
                
                return true;
            }
    }
}