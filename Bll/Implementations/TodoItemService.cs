using AutoMapper;
using BLL.Contracts;
using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Implementations;
using Domain.Models;
using Microsoft.Extensions.Logging;

namespace BLL.Implementations
{
    public class TodoItemService :
        CrudService<TodoItemModel>,
        ITodoItemService
    {
        public TodoItemService(
            ITodoItemDataAccess dataAccess,
            ILogger<CrudService<TodoItemModel>> logger,
            IMapper mapper)
            : base(dataAccess, logger, mapper)
        {
        }
    }
}