﻿using System.Collections.Generic;
using AutoMapper;
using BLL.Contracts;
using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Implementations;
using Domain.Models;
using Microsoft.Extensions.Logging;

namespace BLL.Implementations
{
    public class TodoGroupService :
        CrudService<TodoGroupModel>,
        ITodoGroupService
    {
        public TodoGroupService(
            ITodoGroupDataAccess dataAccess,
            ILogger<CrudService<TodoGroupModel>> logger,
            IMapper mapper)
            : base(dataAccess, logger, mapper)
        {
        }
    }
}