using Domain.Models;

namespace BLL.Contracts
{
    public interface ITodoItemService : ICrudService<TodoItemModel>
    {
    }
}