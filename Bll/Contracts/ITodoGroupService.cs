using Domain.Models;

namespace BLL.Contracts
{
    public interface ITodoGroupService : ICrudService<TodoGroupModel>
    {
    }
}