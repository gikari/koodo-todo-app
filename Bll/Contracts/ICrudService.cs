using System.Collections.Generic;
using Domain.Models;

namespace BLL.Contracts
{
    public interface ICrudService<TModel> where TModel : CrudModel
    {
        void Add(TModel model);
        void Remove(TModel model);
        void Modify(TModel model);
        IEnumerable<TModel> GetAll();
        TModel Get(int id);
    }
}