namespace Client.DTO
{
    public class TodoItemDto : CrudDto
    {
        public TodoGroupDto Group { get; set; }
        public string Contents { get; set; }
        public bool Done { get; set; }
    }
}