﻿namespace Client.DTO
{
    public class TodoGroupDto : CrudDto
    {
        public string Name { get; set; }
    }
}